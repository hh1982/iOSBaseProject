//
//  LoginViewController.m
//  renrenPrint
//
//  Created by hh on 16/7/15.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "LoginViewController.h"
#import "ViewController.h"

@interface LoginViewController ()
{
    UITextField *nameField;
    UITextField *passwdField;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"登录验证";
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"loginReuse"];
    
    CGFloat edge = 25;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44 + 2*edge)];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"马上进入" forState:UIControlStateNormal];
    btn.backgroundColor = COLOR(100, 46, 0);
    btn.frame = CGRectInsetMy(view.frame, 25, 25);
    [btn addTarget:self action:@selector(goLogin:) forControlEvents:UIControlEventTouchUpInside];
    btn.layer.cornerRadius = 4;
    [view addSubview:btn];
    
    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitleColor:RGB(0x787878) forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(forgetPass:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:@"忘记密码请与客服联系" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [btn sizeToFit];
    btn.frame = CGRectMake(self.view.width - 25 - btn.width, view.height - 16, btn.width, btn.height);
    
    [view addSubview:btn];
    self.tableView.tableFooterView = view;
    self.tableView.allowsSelection = NO;
}

//- (void)login:(id)sender
//{
//    [self dismissViewControllerAnimated:YES completion:nil];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    if (indexPath.section == 0) {
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"WD020"]];
    }
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        CGFloat len = [cell.textLabel.text sizeWithAttributes:@{NSFontAttributeName:cell.textLabel.font}].width;
        len += 25;
        if (!nameField) {
            nameField = [[UITextField alloc] initWithFrame:CGRectMake(len, 0, self.view.width - len, 54)];
            nameField.inputAccessoryView = [Helper getInputAccessoryViewForCloseKeyboard];
        }
        nameField.placeholder = @"请输入帐号";
        nameField.text = [DataCache sharedData].loginInfo.客户名称;
#ifdef DEBUG
        nameField.text = @"美天印务";
#endif
        nameField.clearButtonMode = UITextFieldViewModeAlways;
        if ([DataCache sharedData].loginInfo.客户名称) {
            nameField.text = [DataCache sharedData].loginInfo.客户名称;
        }
        [cell.contentView addSubview:nameField];
        cell.separatorInset = UIEdgeInsetsMake(0, len, 0, 0);
        cell.indentationWidth = -len;
        cell.indentationLevel = 1;
    }
    if (indexPath.section == 0 && indexPath.row == 1) {
        if (!passwdField) {
            CGFloat len = [cell.textLabel.text sizeWithAttributes:@{NSFontAttributeName:cell.textLabel.font}].width;
            len += 25;
            passwdField = [[UITextField alloc] initWithFrame:CGRectMake(len, 0, self.view.width - len - 50, 54)];
            passwdField.inputAccessoryView = [Helper getInputAccessoryViewForCloseKeyboard];
            passwdField.secureTextEntry = YES;
        }
        passwdField.placeholder = @"请输入密码";
        passwdField.text = [DataCache sharedData].loginInfo.登陆密码;
#ifdef DEBUG
        passwdField.text = @"123456";
#endif
        if ([DataCache sharedData].loginInfo.登陆密码) {
            passwdField.text = [DataCache sharedData].loginInfo.登陆密码;
        }
        [cell.contentView addSubview:passwdField];
        
        UISwitch *swi = [UISwitch new];
        [swi sizeToFit];
        [swi addTarget:self action:@selector(toggleCanSeePassword:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = swi;
    }
    
    return cell;
}

- (void)goLogin:(id)sender
{
    if (nameField.text.length < 1) {
        SHOW_ALERT(@"请输入用户名");
    }else if (passwdField.text.length < 1){
        SHOW_ALERT(@"请输入密码！");
    }else{
        MBProgressHUD *hud = [Helper showWaitHUDTips:@"正在登录" inView:self.view yOffset:0];
        [DataCache loginWithName:nameField.text password:passwdField.text complete:^(BOOL ok) {
            [hud hide:YES];
            if (ok) {
                [Helper showTips:@"登录成功" forTime:1 inView:[Helper window] yOffset:0];
                [self dismissViewControllerAnimated:YES completion:nil];
                [Helper getTabCon].selectedIndex = 0;
                [[Helper getCurrentViewController] viewWillAppear:YES];
            }else{
//                SHOW_ALERT(@"登录失败！");
            }
        }];
    }
}

- (void)forgetPass:(id)sender
{
    
}

- (void)toggleCanSeePassword:(UISwitch *)swi
{
    passwdField.secureTextEntry = !swi.isOn;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 54;
}

@end
