//
//  DataCache.h
//  niuPei
//
//  Created by hh on 15/6/6.
//  Copyright (c) 2015年 hh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseData.h"
#import "LoginResultData.h"

#ifndef DataCache_h
#define DataCache_h

#define PAGENUM 15

#define UserLogoutNotification @"UserHasLogout"

@interface DataCache : NSObject
{

}

+ (instancetype)sharedData;

// 因建立dateFormatter成本高，所以利用单例模式，全局只用一个
@property (strong,nonatomic) NSDateFormatter *dateFormtter;

@property (assign,nonatomic) BOOL hasLogin;

@property (strong,nonatomic) LoginData *loginInfo;
@property (copy,nonatomic) NSString *token;
@property (strong,nonatomic) NSString *baseURL;

- (void)saveState;

+ (void)checkIfLogin:(UIViewController *)con;

#pragma mark - 登录方法
// /wx/wx_getToken.jspx
+ (void)loginWithName:(NSString *)name password:(NSString *)passwd complete:(void (^)(BOOL))handle;

#pragma mark - 根据URL判断图片是否存在
+ (void)imgURLExists:(NSString *)urlString complete:(void (^)(BOOL))handle;

@end

#endif
