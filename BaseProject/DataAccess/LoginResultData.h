//
//  LoginResultData.h
//  huaSheng
//
//  Created by hh on 15/10/17.
//  Copyright © 2015年 hh. All rights reserved.
//

#import "BaseData.h"

@interface LoginData : JSONModel
@property (strong,nonatomic) NSString *客户名称;
@property (strong,nonatomic) NSString *客户代码;
@property (strong,nonatomic) NSString *登陆密码;
@property (strong,nonatomic) NSString *权限;
@property (assign,nonatomic) CGFloat 透支额度;
@property (assign,nonatomic) CGFloat 剩余金额;
@property (assign,nonatomic) NSInteger 序号;
@property (strong,nonatomic) NSString *客服人员;

@property (strong, nonatomic) NSString *城市码; // 部门
@property (strong, nonatomic) NSString *区域码; // 后工工种
@property (strong,nonatomic) NSString *会员级别;

@property (strong, nonatomic) NSString *用户状态;

@property (strong, nonatomic) NSString *token;
@end

@interface LoginResultData : BaseData

@property (strong,nonatomic) LoginData *record;

@end
