
//
//  JSONModel.m
//  niuPei
//
//  Created by hh on 15/6/7.
//  Copyright (c) 2015年 hh. All rights reserved.
//

#import "JSONModel.h"
#import <objc/runtime.h>

@implementation JSONModel 

-(instancetype)initWithDictionary:(NSMutableDictionary *)jsonDictionary
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:jsonDictionary];
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
//    NSLog(@"key:%@",key);
    if ([value isKindOfClass:[NSArray class]]) {
        Class dataClass = [self getSubDataClassInArrayForKey:key];
        NSMutableArray *arr = [NSMutableArray new];
        for (NSDictionary *json in value) {
            if ([json isKindOfClass:[NSDictionary class]]) {
                id o = [[dataClass alloc] initWithDictionary:json];
                [arr addObject:o?:@""];
            }else{
                [arr addObject:json];
            }
        }
        [super setValue:arr forKey:key];
    }else if ([value isKindOfClass:[NSDictionary class]]){
        objc_property_t pt = class_getProperty(self.class, [key cStringUsingEncoding:NSUTF8StringEncoding]);
        NSString *clsString = [NSString stringWithCString:property_getAttributes(pt) encoding:NSUTF8StringEncoding];
        NSRange endR = [clsString rangeOfString:@","];
        NSUInteger end = endR.location - 1;
        clsString = [clsString substringWithRange:NSMakeRange(3, end - 3)];
//         NSLog(@"class:%@",clsString);
        Class dataClass = NSClassFromString(clsString);
        if (dataClass == NSDictionary.class || dataClass == [NSString class]) {
            [super setValue:value forKey:key];
        }else{
            [super setValue:[[dataClass alloc] initWithDictionary:value] forKey:key];
        }
    }else{
        [super setValue:value forKey:key];
    }
}

- (NSString *)description
{
    NSArray *propertyList = [self getPropertyListForEncode];
    NSMutableString *str = [NSMutableString new];
    [str appendString:@"\n{"];
    for (NSString *propertyKey in propertyList) {
        id value = [self valueForKey:propertyKey];
        [str appendFormat:@"\t%@:%@\n",propertyKey,value];
//        NSLog(@"%@:%@",propertyKey,value);
    }
    [str appendString:@"}"];
    return str;
}

- (NSDictionary *)getSubDataClassInArrayForKey:(NSString *)key
{
    return nil;
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        [self setValue:value forKey:@"ID"];
        return;
    }
    NSLog(@"Undefined Key: %@:%@",key,value);
}

- (NSArray *)getPropertyListForEncode
{
    // test:
//    NSLog(@"<%@ properties>:",self.class);
    Class cls = self.class;
    NSMutableArray *arr = [NSMutableArray new];
    while (![cls isEqual:JSONModel.class]) {
        unsigned int count;
        objc_property_t *properties = class_copyPropertyList(cls, &count);
        if (properties == NULL) {
            continue;
        }
        for (int i = 0; i < count; i++) {
            NSString *key = @(property_getName(properties[i]));
//            NSLog(@"%@",key);
            [arr addObject:key];
        }
        
        free(properties);
        
        cls = cls.superclass;
    }
    return arr;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    NSArray *propertyList = [self getPropertyListForEncode];
    for (NSString *propertyKey in propertyList) {
        // may have bugs: property maybe BOOL、int etc
        id value = [self valueForKey:propertyKey];
//        NSLog(@"%@:%@",propertyKey,value);
        [aCoder encodeObject:value forKey:propertyKey];
//        if ([self isKindOfClass:[NSArray class]]) {
//            if (value) {
//                [aCoder encodeObject:value forKey:propertyKey];
//            }
//        }else{
//            [self setValue:value forKey:propertyKey];
//        }
    }
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    NSArray *propertyList = [self getPropertyListForEncode];
    for (NSString *propertyKey in propertyList) {
        // may have bugs: property maybe BOOL、int etc
        id value = [aDecoder decodeObjectForKey:propertyKey];
//        NSLog(@"%@:%@",propertyKey,value);
//        if ([self isKindOfClass:[NSArray class]]) {
//            if (value) {
//                [self setValue:value forKey:propertyKey];
//            }
//        }else{
            [self setValue:value forKey:propertyKey];
//        }
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [self.class allocWithZone:zone];
    NSArray *arr = [self getPropertyListForEncode];
    for (NSString *key in arr) {
        NSLog(@"%@",key);
        id value = [self valueForKey:key];
        [copy setValue:[value copy] forKey:key];
    }
    return copy;
}

- (id)mutableCopyWithZone:(NSZone *)zone
{
    id mutableCopy = [self.class allocWithZone:zone];
    NSArray *arr = [self getPropertyListForEncode];
    for (NSString *key in arr) {
        id value = [self valueForKey:key];
        [mutableCopy setValue:[value mutableCopy] forKey:key];
    }
    return mutableCopy;
}

@end
