//
//  main.m
//  BaseProject
//
//  Created by 黄海 on 2017/3/1.
//  Copyright © 2017年 黄海. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
