//
//  Helper.h
//  JiPei
//
//  Created by hh on 16/1/29.
//  Copyright © 2016年 hh. All rights reserved.
//

#ifdef DEBUG
#define NSLog(s,...) NSLog( @"<%@:(%d)> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s),##__VA_ARGS__] )
#else
#define NSLog(s,...)
#endif

#ifdef DEBUG
#define SHOW_DEBUG(_message_) UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨信息" message:_message_ delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定" ,nil]; [alert show];
#else
#define SHOW_DEBUG(_message_)
#endif

#define SHOW_ALERT(_message_) UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:_message_ delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定" ,nil]; \
[alert show];\

#define SHOW_TIPS(_message_,_offset_) [Helper showTips:_message_ forTime:1 inView:[Helper window] yOffset:_offset_];

#define STRING_SIZE_BY_WIDTH(_width_, _string_, _fontSize_) [_string_ boundingRectWithSize:CGSizeMake(_width_, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:_fontSize_]} context:nil].size

#define STRING_SIZE(_string_, _font_) [_string_ sizeWithAttributes:@{NSFontAttributeName:_font_}]

//获取rgb 色值
#define RGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define VERSION_KEY @"version_key"
#define LOADING_MSG @"正在加载..."
#define EMPTY_DATA_MSG @"暂时没有数据！"
#define SHOW_HUD MBProgressHUD *hud = [Helper showWaitHUDTips:LOADING_MSG inView:[Helper window] yOffset:0];
#define USERNAME_KEY @"userName"
#define PASSWORD_KEY @"password"
#define DB_NAME @"msgs.db"
#define MY_HEADER_IMAGE_KEY [@"my_header_image_key_" stringByAppendingString:[DataCache sharedData].loginInfo.memberId?:@""]
#define HAS_ONBOARD_RUNS @"OnboardHasRuns"
#define MAX_SEND_TXT_LENGTH 200
#define BASE_URL @"BASE_URL"

#define MAINCOLOR [UIColor colorWithRed:0 green:0.75 blue:0.78 alpha:1]
#define GRAYCOLOR [UIColor colorWithWhite:0.95 alpha:1]

// 颜色百分比
#define COLOR(r,g,b) [UIColor colorWithRed:r/100.0 green:g/100.0 blue:b/100.0 alpha:1]
#define WHITE(r) [UIColor colorWithWhite:r alpha:1]

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "TranlucentToolbar.h"

CGRect CGRectInsetMy(CGRect rect, CGFloat dx, CGFloat dy);

@interface Helper : NSObject

+ (NSString *)getDateString:(NSDate *)date;
+ (NSString *)getCurrentDateString;
+ (NSString *)getDateStringForDays:(int)days;

+ (CGSize)sizeOfText:(NSString *)text font:(UIFont *)font;
+ (CGFloat)textHeightWithWidth:(CGFloat)width font:(UIFont *)font text:(NSString *)text;
+ (UIToolbar *)getInputAccessoryViewForCloseKeyboard;
+ (UIToolbar *)getInputAccessoryViewForCloseKeyboardWithTitle:(NSString *)title;
+ (TranlucentToolbar *)getClearInputAccessoryViewForCloseKeyboard;
+ (UIWindow *)window;
+ (AppDelegate *)app;

+ (void)showTips:(NSString *)tips forTime:(NSTimeInterval)time inView:(UIView *)view yOffset:(CGFloat)y;
+ (MBProgressHUD *)showWaitHUDTips:(NSString *)tips inView:(UIView *)view yOffset:(CGFloat)y;

+ (UIViewController *)getCurrentViewController;

+ (NSString *)getParamByKey:(NSString *)key inParams:(NSString *)params;

+ (void)showLoginControllerIn:(UIViewController *)conParent;

+ (NSString *)md5:(NSString *)text;

+ (NSString *)getRandomString:(int)num;
+ (NSString *)getIPAddress;

+ (NSString *)floatToString:(CGFloat)num reservCount:(int)count;

+ (UITabBarController *)getTabCon;

+ (BOOL)isPostProcess:(NSString *)name;

@end
