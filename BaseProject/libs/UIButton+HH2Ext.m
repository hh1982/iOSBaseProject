//
//  UIButton+HH2Ext.m
//  JiPei
//
//  Created by hh on 16/1/27.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "UIButton+HH2Ext.h"

@implementation UIButton (HH2Ext)

- (void)setImageRightToTitle:(CGFloat)space
{
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}];
    
    self.imageEdgeInsets = UIEdgeInsetsMake( 0, titleSize.width + space, 0, -titleSize.width);
    self.titleEdgeInsets = UIEdgeInsetsMake( 0, -imageSize.width, 0, imageSize.width);
}

- (void)setImageRightToTitle
{
    return [self setImageRightToTitle:8];
}

- (void)setImageOntoTitle:(float)spacing {
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}];
    CGFloat totalHeight = (imageSize.height + titleSize.height + spacing);
    
    self.imageEdgeInsets = UIEdgeInsetsMake( -(totalHeight - imageSize.height), 0.0, 0.0, -titleSize.width);
    self.titleEdgeInsets = UIEdgeInsetsMake( 0.0, -imageSize.width, -(totalHeight - titleSize.height), 0.0);
}

- (void)setImageOntoTitle{
    const int SPACING = 6.0f;
    [self setImageOntoTitle:SPACING];
}

@end
