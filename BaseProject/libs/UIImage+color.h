//
//  UIImage+color.h
//  renrenPrint
//
//  Created by hh on 16/7/27.
//  Copyright © 2016年 hh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (color)

- (UIImage *)imageWithColor:(UIColor *)color;
- (UIImage *) scaleToScale:(float)scaleSize;
- (UIImage *) reSizeToSize:(CGSize)reSize;

// 裁剪图片
-(UIImage *)getImageFromRect:(CGRect)myImageRect;
// 合并2张图
- (UIImage *)addImageOnSelf:(UIImage *)image1;

+ (UIImage*) captureView:(UIView *)theView;

@end
