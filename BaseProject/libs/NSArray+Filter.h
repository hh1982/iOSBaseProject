//
//  NSArray+Filter.h
//  renrenPrint
//
//  Created by hh on 16/7/19.
//  Copyright © 2016年 hh. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef BOOL(^BoolBlock)(id obj);
typedef void(^VoidBlock)(id obj);

typedef BOOL(^FilterArrayBlock)(id obj);
typedef BOOL(^FilterIndexBlock)(id obj, NSInteger idx);
typedef id(^MapArrayBlock)(id obj);
typedef id(^ReduceArrayBlock)(id obj, id obj2);
typedef BOOL(^FindObjBlock)(id obj);
typedef void(^ForEachBlock)(id obj);
typedef void(^ForEach)(id obj, NSInteger idx);
typedef id(^Map)(id obj, NSInteger idx);

typedef void(^IntBlock)(NSInteger row, NSInteger colum, NSInteger idx);

@interface NSArray (Filter)

- (NSArray *)filterArrayWithBlock:(FilterArrayBlock)block;

- (NSArray *)filter:(FilterIndexBlock)fBlock;
- (NSArray *)filter:(FilterArrayBlock)fBlock thenMap:(MapArrayBlock)mBlock;
- (void)filter:(FilterArrayBlock)fBlock thenForEachDo:(ForEachBlock)dBlock;

- (NSArray *)uniqArray;

- (NSArray *)map:(Map)block;
- (NSArray *)mapArrayWithBlock:(MapArrayBlock)block;
- (id)reduceArrayWithBlock:(ReduceArrayBlock)block initValue:(id)origin;

- (NSInteger)findObjIndexWithBlock:(FindObjBlock)block;
- (void)forEachDo:(ForEachBlock)block;
- (void)forEach:(ForEach)block;

- (BOOL)some:(BoolBlock)block;
- (BOOL)every:(BoolBlock)block;

- (void)gridForCount:(NSInteger)count colums:(NSInteger)cols do:(IntBlock)block;

+ (NSArray *)arrayFrom:(NSInteger)from to:(NSInteger)to;
+ (NSArray *)arrayFrom:(NSInteger)from to:(NSInteger)to step:(NSInteger)step;
+ (NSArray *)arrayFor:(id)obj count:(NSInteger)count;

@end
