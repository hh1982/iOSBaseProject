//
//  UIButton+HH2Ext.h
//  JiPei
//
//  Created by hh on 16/1/27.
//  Copyright © 2016年 hh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (HH2Ext)

//imageView在上,label在下
- (void)setImageOntoTitle:(float)space;
- (void)setImageOntoTitle;

- (void)setImageRightToTitle:(CGFloat)space;
- (void)setImageRightToTitle;

@end
